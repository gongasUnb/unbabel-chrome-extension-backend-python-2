import sqlite3
from sqlite3 import Error
import datetime

from oauth2client.client import OAuth2Credentials


def create_connection(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)


def execute_sql(conn, sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param sql: a sql statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(sql)
        conn.commit()
    except Error as e:
        print(e)


def execute_sql_with_data(conn, sql, data):
    try:
        c = conn.cursor()
        c.execute(sql, data)
        conn.commit()
    except Error as e:
        print(e)


def init(db_file):
    conn = create_connection(db_file)
    sql = """
        CREATE TABLE IF NOT EXISTS user_creds(
        user_id text PRIMARY KEY,
        email text NOT NULL,
        token_uri text NOT NULL,
        client_id text NOT NULL,
        client_secret text NOT NULL,
        token_expiry timestamp NOT NULL,
        access_token text NOT NULL)
    """
    try:
        c = conn.cursor()
        c.execute(sql)
    except Error as e:
        print(e)


def store_user(db_file, user_id, email, token_uri, client_id, client_secret, token_expiry, access_token):
    conn = create_connection(db_file)
    sql = """
            INSERT INTO user_creds
            VALUES (?, ?, ?, ?, ?, ?, ?);
        """
    data = (user_id, email, token_uri, client_id, client_secret, token_expiry, access_token)
    execute_sql_with_data(conn, sql, data)


def get_by_user_id(db_file, user_id):
    conn = create_connection(db_file)
    sql = """
        SELECT * FROM user_creds
        WHERE user_id = ?
    """
    try:
        c = conn.cursor()
        c.execute(sql, (user_id,))
        return c.fetchall()
    except Error as e:
        print(e)


def get_user_by_email(db_file, email):
    conn = create_connection(db_file)
    sql = """
        SELECT * FROM user_creds
        WHERE email = ?
    """
    try:
        c = conn.cursor()
        c.execute(sql, (email,))
        return c.fetchall()
    except Error as e:
        print(e)


def get_all_users(db_file):
    conn = create_connection(db_file)
    sql = """
            SELECT * FROM user_creds
        """
    try:
        c = conn.cursor()
        c.execute(sql)
        return c.fetchall()
    except Error as e:
        print(e)


def main(credentials):
    token_uri = credentials.token_uri
    client_id = credentials.client_id
    client_secret = credentials.client_secret
    token_expiry = credentials.token_expiry
    access_token = credentials.access_token

    user_id = '107821910789878122628'
    email = 'goncalo.patrocinio@unbabel.com'

    file_name = 'users.db'
    init(file_name)
    store_user(file_name, user_id, email, token_uri, client_id, client_secret, token_expiry, access_token)

    print(get_user_by_email(file_name, email))


if __name__ == '__main__':
    credentials = OAuth2Credentials('ya29.a0AfH6SMBfNhBFKWfLzD9yer81M_4-5Wyw9GVFkJm1XI6vnxbrW3YzRC68Mt2dppfirapPCLJuCAYqFpLlPHFqnaHEdd-dG-wchEYpL8VSNjjbaVxYbU30uzqAxXXGNStRLK5eBePt7s5FJ4WzEXbXJo5yQsGmurrDFmRbby980oiQ', '806744349182-ahfuva394hcukjpf52fr29ef2ht7oh8q.apps.googleusercontent.com', 'PR32Asa1VGaCYjNSq4Ulu4JV', None, datetime.datetime(2020, 11, 24, 21, 14, 11, 718251), 'https://oauth2.googleapis.com/token', None, revoke_uri=None, id_token=None, token_response=None)
    main(credentials)
