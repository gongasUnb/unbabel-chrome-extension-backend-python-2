import requests
import json


def get_languages(auth):
    url = "https://aws-staging.unbabel.com/tapi/v2/language_pair"
    # url = "https://api.unbabel.com/tapi/v2/language_pair"

    r = requests.get(url,
                     headers={'Authorization': auth})
    return r.text


def get_languages_fake():
    return {
        "ar": "Arabic",
        "bg": "Bulgarian",
        "cs": "Czech",
        "da": "Danish",
        "de": "German",
        "el": "Greek",
        "en": "English",
        "es": "Spanish",
        "es-latam": "Spanish(Latam)",
        "fi": "Finnish",
        "fr": "French",
        "hi": "Hindi",
        "hu": "Hungarian",
        "id": "Indonesian",
        "it": "Italian",
        "ja": "Japanese",
        "ko": "Korean",
        "la": "Fake English",
        "nl": "Dutch",
        "no": "Norwegian",
        "pl": "Polish",
        "pt": "Portuguese",
        "pt-br": "Portuguese(BR)",
        "ro": "Romanian",
        "ru": "Russian",
        "sv": "Swedish",
        "th": "Thai",
        "tr": "Turkish",
        "vi": "Vietnamese",
        "zh-CN": "Chinese (simplified)",
        "zh-TW": "Chinese (Traditional)"
    }


def get_distinct_languages(lang_json):
    result = {}
    for x in lang_json["objects"]:
        language = x["lang_pair"]["source_language"]
        short_name = language["shortname"]
        name = language["name"]
        if short_name not in result:
            result[short_name] = name
    return result


def testing():
    return get_languages_fake()


def send_distinct_languages(auth):
    return get_distinct_languages(json.loads(get_languages(auth)))
