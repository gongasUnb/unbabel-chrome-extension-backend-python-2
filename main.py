import base64
import json

import requests
from idna import unicode

from languages import send_distinct_languages
from gmail.gmail import update_draft
from gmail.api import *
from flask import Flask, request
from flask_cors import CORS, cross_origin
from db.db import *

app = Flask(__name__)
cors = CORS(app)

# initialize the db first
init(DB_FILENAME)


@app.route('/', methods=['GET', 'POST'])
@cross_origin()
def default():
    if request.method == 'GET':
        return 'hello, world'
    else:
        return request.json


@app.route('/languages')
@cross_origin()
def languages():
    auth = request.headers['Authorization']
    return send_distinct_languages(auth)


@app.route('/translate', methods=['GET'])
@cross_origin()
def translate():
    return {"text": 'e uma maquina',
            "fullText": 'o tomas e uma maquina',
            "from": 'pt',
            "to": 'en',
            "translated": 'is a machine'}


@app.route('/translate_mt', methods=['POST'])
@cross_origin()
def translate_mt():
    auth = request.headers['Authorization']
    # url = 'https://aws-staging.unbabel.com/tapi/v2/mt_translation'
    url = 'https://api.unbabel.com/tapi/v2/mt_translation'

    post = requests.post(url, json=request.json, headers={'Authorization': auth})
    res_post = post.json()
    print(res_post)
    val = 0
    while val < 10:
        val = val + 1
        print("another try - " + str(val))
        get = requests.get(url + '/' + res_post.get("uid"), headers={'Authorization': auth})
        res_get = get.json()
        print(res_get)
        if res_get.get('status') == "deliver_ok":
            return {"text": res_post.get('text'),
                    "fullText": res_post.get('fullText'),
                    "translated": res_get.get("translatedText")}


@app.route('/translate_human', methods=['POST'])
@cross_origin()
def translate_human():
    auth = request.headers['Authorization']
    # url = 'https://aws-staging.unbabel.com/tapi/v2/mt_translation'
    url = 'https://api.unbabel.com/tapi/v2/translation'

    # only request if draftId is valid
    print(request.json)
    print(request.json.get('draftId'))
    if request.json.get('draftId') == '':
        return 'No Draft Id in JSON', 400

    # replace this weird char '\xa0' to a blank space
    val = request.json.get('text')
    val = val.replace('\xa0', ' ')
    request.json.update({"text": val})
    val = request.json.get('fullText')
    val = val.replace('\xa0', ' ')
    request.json.update({"fullText": val})
    print(request.json)

    # only request if user has access to gmail API
    creds = get_stored_credentials(request.json['email'])
    if creds is None:
        return 'No Credentials Found', 400

    post = requests.post(url, json=request.json, headers={'Authorization': auth})
    print(post.json)
    if post.status_code != 201:
        return 'Error requesting translation', 400

    return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}


@app.route('/receive_callback/<draft_id>/<email>/<full_text>/<auto>', methods=['POST'])
@cross_origin()
def receive_callback(draft_id, email, full_text, auto):
    # decode email
    email = unquote(email)

    # get the translated text and strip to make sure its ok
    req = request.form.to_dict()
    print(req)
    translated_text = req.get('translated_text').strip()
    text = req.get('text').strip()

    # decode the received text
    decoded_full_text = base64.urlsafe_b64decode(full_text).decode("utf-8", errors='ignore').strip()

    # replace the selected part of the text to the translation received
    replaced_text = decoded_full_text.replace(text, translated_text)

    # create service for the gmail API
    creds = get_stored_credentials(email)
    if creds is None:
        return 'No Credentials Found', 400
    service = build_service(creds)

    # update the draft via the API
    update_draft(replaced_text, draft_id, auto, service)

    # return success
    return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}


@app.route('/oauth2')
@cross_origin()
def oauth2():
    # get credentials from the url requested
    credentials = creds_from_uri(request.url)

    # get user id and store it if not exists
    user_info = get_user_info(credentials)
    user_id = user_info.get('id')
    email = user_info.get('email')
    store_credentials(user_id, email, credentials)

    return 'Authentication Received, You can close this window now!', 200


if __name__ == '__main__':
    app.run()
