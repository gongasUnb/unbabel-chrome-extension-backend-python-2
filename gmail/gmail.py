from __future__ import print_function
import base64
import pickle
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import os.path

from gmail.api import *

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.compose',
          'https://www.googleapis.com/auth/gmail.metadata']


def decode_msg(msg):
    if msg.get("payload").get("body").get("data"):
        return base64.urlsafe_b64decode(msg.get("payload").get("body").get("data").encode("ASCII")).decode("utf-8")
    return msg.get("snippet")


def encode_msg(msg):
    message_bytes = msg.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')
    return base64_message


def create_test_message(sender, to, subject, body):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = to
    plain_text = body
    html_text = """\
                <html>
                  <head></head>
                  <body>
                    %s
                    <p></p>
                    <p>Translated using <a href="http://www.unbabel.com/">Unbabel</a></p>
                  </body>
                </html>
                """ % body

    # Swapping the following two lines results in Gmail generating HTML
    # based on plaintext, as opposed to generating plaintext based on HTML
    msg.attach(MIMEText(plain_text, 'plain'))
    msg.attach(MIMEText(html_text, 'html'))

    # print('-----\nHere is the message:\n\n{m}'.format(m=msg))
    encoded = base64.urlsafe_b64encode(msg.as_string().encode('UTF-8')).decode('UTF-8')
    return {'raw': encoded}


def send_draft(service, draft_id):
    draft_body = {
        "id": draft_id
    }
    try:
        result = service.users().drafts().send(userId="me", body=draft_body).execute()
        print(result.get("id"))
        print(result.get("labelIds"))
        return result
    except:
        print('something went wrong trying to send draft')
        return


def update_draft(body, draft_id, auto, service):

    # Call the Gmail API to get draft from the id
    result = service.users().drafts().get(userId="me", id=draft_id, format="full").execute()

    # Get the sender, to, subject and body from the message
    draft = result.get('message', [])
    # body_decrypted = decode_msg(draft)
    subject = ''
    from_sender = ''
    to_sender = ''
    for x in draft.get('payload').get('headers'):
        name = x.get('name')
        if name == 'Subject':
            subject = x.get('value')
        if name == 'From':
            from_sender = x.get('value')
        if name == 'To':
            to_sender = x.get('value')

    # Create a new raw body for the message
    updated_raw = create_test_message(from_sender, to_sender, subject, body)
    updated_body = {
        "id": draft_id,
        "message": updated_raw
    }

    # Edit the received draft
    try:
        result2 = service.users().drafts().update(userId="me", id=draft_id, body=updated_body).execute()
        print('updated!')
    except:
        print('something went wrong updating draft')

    # if auto is true try to send email
    if auto == 'true':
        send_draft(service, draft_id)
        print("sent")
